<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = ["category_id", "user_id", "title", "content", "image", "slug"];

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function setSlugAttribute($value) {
        $this->attributes["slug"] = Str::slug($value);
    }

    public function tags() {
        return $this->belongsToMany(Tag::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}