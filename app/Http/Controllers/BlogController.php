<?php

namespace App\Http\Controllers;

use App\Tag;
use App\User;
use App\Post;
use App\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with(["category", "tags", "user"])->orderBy("created_at", "DESC")->paginate(10);
        $categories = Category::orderBy("created_at", "DESC")->get();
        $tags = Tag::orderBy("created_at", "DESC")->get();

        return view("blog.index", compact("posts", "categories", "tags"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $slug)
    {
        $post = Post::findOrFail($id)->where("slug", $slug)->first();
        $categories = Category::orderBy("created_at", "DESC")->get();
        $tags = Tag::orderBy("created_at", "DESC")->get();

        return view("blog.show", compact("post", "categories", "tags"));
    }

    public function post_category_list(Category $category) {
        $posts = $category->posts()->paginate(10);
        $categories = Category::orderBy("created_at", "DESC")->get();
        $tags = Tag::orderBy("created_at", "DESC")->get();

        return view("blog.index", compact("posts", "categories", "tags"));
    }

    public function post_tag_list(Tag $tag) {
        $posts = $tag->posts()->paginate(10);
        $categories = Category::orderBy("created_at", "DESC")->get();
        $tags = Tag::orderBy("created_at", "DESC")->get();

        return view("blog.index", compact("posts", "categories", "tags"));
    }

    public function search(Request $request) {
        $posts = Post::orderBy("created_at", "DESC")->where("title", $request->search)->orWhere("title", "like", "%" . $request->search . "%")->paginate(10);
        $categories = Category::orderBy("created_at", "DESC")->get();
        $tags = Tag::orderBy("created_at", "DESC")->get();

        return view("blog.index", compact("posts", "categories", "tags"));
    }
}