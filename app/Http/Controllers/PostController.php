<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Post;
use App\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->user_role) {
            $posts = Post::with(["category", "tags"])->orderBy("created_at", "DESC")->paginate(10);
        } else {
            $posts = Post::with(["category", "tags"])->where("user_id", Auth::user()->id)->orderBy("created_at", "DESC")->paginate(10);
        }

        return view("admin.post.index", compact("posts"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::orderBy("created_at", "DESC")->get();
        $categories = Category::orderBy("created_at", "DESC")->get();

        return view("admin.post.create", compact("tags", "categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "category_id" => "required|integer|exists:categories,id",
            "title" => "required|string",
            "content" => "required",
            "image" => "required|image|mimes:png,jpeg,jpg"
        ]);

        if ($request->hasFile("image")) {
            $file = $request->file("image");
            $filename = time() . "." . $file->getClientOriginalExtension();
            
            $file->storeAs("public/posts", $filename);

            $post = Post::create([
                "category_id" => $request->category_id,
                "user_id" => Auth::id(),
                "title" => $request->title,
                "content" => $request->content,
                "image" => $filename,
                "slug" => $request->title
            ]);

            $post->category()->attach($request->category_id);
            $post->tags()->attach($request->tags);
        }

        return redirect(route("admin.post.index"))->with(["success" => "Post created successfully!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $tags = Tag::orderBy("created_at", "DESC")->get();
        $categories = Category::orderBy("created_at", "DESC")->get();

        return view("admin.post.edit", compact("post", "tags", "categories"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            "category_id" => "required|integer|exists:categories,id",
            "title" => "required|string",
            "content" => "required",
            "image" => "nullable|image|mimes:png,jpeg,jpg"
        ]);
        
        $request->request->add(["slug" => $request->title]);
        
        $filename = $post->image;

        if ($request->hasFile("image")) {
            $file = $request->file("image");
            $filename = time() . "." . $file->getClientOriginalExtension();
            
            $file->storeAs("public/posts", $filename);

            File::delete(storage_path('app/public/posts/' . $post->image));
        }

        $post->update([
            "category_id" => $request->category_id,
            "title" => $request->title,
            "content" => $request->content,
            "image" => $filename,
            "slug" => $request->title
        ]);

        $post->category()->detach();
        $post->tags()->detach();

        $post->category()->attach($request->category_id);
        $post->tags()->attach($request->tags);

        return redirect(route("admin.post.index"))->with(["success" => "Post created successfully!"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::withTrashed()->where("id", $id)->first();

        File::delete(storage_path('app/public/posts/' . $post->image));

        $post->forceDelete();

        return redirect()->back()->with(["success" => "Post deleted successfully!"]);
    }

    public function soft_delete(Post $post)
    {
        $post->delete();

        return redirect(route("admin.post.index"))->with(["success" => "Post deleted successfully!"]);
    }

    public function trashed_posts() {
        $posts = Post::onlyTrashed()->paginate(10);

        return view("admin.post.trashed", compact("posts"));
    }

    public function restore($id) {
        $post = Post::withTrashed()->where("id", $id)->first();
        $post->restore();

        return redirect()->back()->with(["success" => "Post restored successfully!"]);
    }
}