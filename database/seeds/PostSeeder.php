<?php

use App\Post;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::create([
            "category_id" => 7,
            "title" => "Laravel 7 Full SEO Blog Tutorial",
            "content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum laoreet tempor maximus. Aenean imperdiet elementum nisi, in viverra augue rutrum sit amet. Proin tincidunt faucibus tortor et iaculis. Pellentesque placerat lectus nibh, non semper elit sodales at. Morbi condimentum purus mauris, sit amet laoreet nunc faucibus vitae. Donec ultricies vitae odio sit amet interdum. Vestibulum gravida nisl sed viverra mattis. Etiam vitae congue libero. Donec eros erat, elementum vitae tortor a, ornare efficitur tortor. Praesent eu mi augue.",
            "image" => "https://images.unsplash.com/photo-1542435503-956c469947f6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1567&q=80"
        ]);

        Post::create([
            "category_id" => 7,
            "title" => "Flask Simple Blog Tutorial",
            "content" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum laoreet tempor maximus. Aenean imperdiet elementum nisi, in viverra augue rutrum sit amet. Proin tincidunt faucibus tortor et iaculis. Pellentesque placerat lectus nibh, non semper elit sodales at. Morbi condimentum purus mauris, sit amet laoreet nunc faucibus vitae. Donec ultricies vitae odio sit amet interdum. Vestibulum gravida nisl sed viverra mattis. Etiam vitae congue libero. Donec eros erat, elementum vitae tortor a, ornare efficitur tortor. Praesent eu mi augue.",
            "image" => "https://images.unsplash.com/photo-1542435503-956c469947f6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1567&q=80"
        ]);
    }
}