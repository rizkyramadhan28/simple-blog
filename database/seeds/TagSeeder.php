<?php

use App\Tag;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::create([
            "name" => "Laravel",
            "slug" => Str::slug("laravel")
        ]);

        Tag::create([
            "name" => "Python",
            "slug" => Str::slug("python")
        ]);
    }
}