<?php

use App\Category;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            "name" => "Web Programming",
            "slug" => Str::slug("Web Programming")
        ]);

        Category::create([
            "name" => "Natural Language Processing",
            "slug" => Str::slug("Natural Language Processing")
        ]);
    }
}