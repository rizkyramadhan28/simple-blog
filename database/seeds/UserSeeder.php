<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "Rizky Ramadhan",
            "email" => "rizky_ramadhan28@yahoo.com",
            "password" => bcrypt("Shady!@#123")
        ]);
    }
}