@extends('layouts.admin')

@section('title')
<title>Admin Dashboard</title>
@endsection

@section('content-title')
<h1>Dashboard</h1>

<div class="section-header-breadcrumb">
    <div class="breadcrumb-item">Dashboard</div>
</div>
@endsection

@section('content')
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae, molestiae, doloremque nesciunt totam doloribus
    ut voluptas nulla maxime provident consectetur dolores consequatur, ea soluta deleniti in fuga harum porro dolor?
</p>
@endsection