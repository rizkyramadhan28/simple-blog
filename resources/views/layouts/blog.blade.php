<!DOCTYPE html>
<html lang="en">

<head>
    @yield('blog-title')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/blog/css/open-iconic-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/blog/css/animate.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/blog/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/blog/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/blog/css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/blog/css/aos.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/blog/css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/blog/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/blog/css/jquery.timepicker.css') }}">


    <link rel="stylesheet" href="{{ asset('assets/blog/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/blog/css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/blog/css/style.css') }}">
</head>

<body>

    <div id="colorlib-page">
        <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>

        @include('layouts.blog.sidebar')

        <div id="colorlib-main">
            <section class="ftco-section ftco-no-pt ftco-no-pb">
                <div class="container">
                    <div class="row d-flex">
                        <div class="col-xl-8 py-5 px-md-5">
                            <div class="row pt-md-4">
                                @yield('content')
                            </div>

                            @yield('content-pagination')
                        </div>

                        <div class="col-xl-4 sidebar ftco-animate bg-light pt-5">
                            @include('layouts.blog.search')

                            @include('layouts.blog.category', $categories)

                            @include('layouts.blog.popular_post')

                            @include('layouts.blog.tag', $tags)

                        </div><!-- END COL -->
                    </div>
                </div>
            </section>
        </div><!-- END COLORLIB-MAIN -->
    </div><!-- END COLORLIB-PAGE -->

    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00" /></svg></div>


    <script src="{{ asset('assets/blog/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/blog/js/jquery-migrate-3.0.1.min.js') }}"></script>
    <script src="{{ asset('assets/blog/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/blog/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/blog/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('assets/blog/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/blog/js/jquery.stellar.min.js') }}"></script>
    <script src="{{ asset('assets/blog/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/blog/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/blog/js/aos.js') }}"></script>
    <script src="{{ asset('assets/blog/js/jquery.animateNumber.min.js') }}"></script>
    <script src="{{ asset('assets/blog/js/scrollax.min.js') }}"></script>
    <script src="{{ asset('assets/blog/js/main.js') }}"></script>

    @yield('custom-js')
</body>

</html>