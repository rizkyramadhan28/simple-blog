<div class="sidebar-box pt-md-4">
    <form action="{{ route('blog.post.search') }}" class="search-form">
        <div class="form-group">
            <span class="icon icon-search"></span>
            <input name="search" type="text" class="form-control" placeholder="Type a keyword and hit enter">
        </div>
    </form>
</div>