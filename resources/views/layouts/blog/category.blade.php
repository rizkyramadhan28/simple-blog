<div class="sidebar-box ftco-animate">
    <h3 class="sidebar-heading">Categories</h3>
    <ul class="categories">
        @foreach ($categories as $category)
        <li><a href="{{ route('blog.post.category_list', $category->slug) }}">{{ $category->name }}</a></li>
        @endforeach
    </ul>
</div>