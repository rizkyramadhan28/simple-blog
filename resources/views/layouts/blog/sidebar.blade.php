<aside id="colorlib-aside" role="complementary" class="js-fullheight">
    <nav id="colorlib-main-menu" role="navigation">
        <ul>
            <li class="colorlib-active"><a href="{{ route('blog.index') }}">Home</a></li>
            <li><a href="about.html">About</a></li>
            <li><a href="contact.html">Contact</a></li>
        </ul>
    </nav>

    <div class="colorlib-footer">

        <h1>Rizky Ramadhan</h1>

        <p class="pfooter">
            Copyright &copy;<script>
                document.write(new Date().getFullYear());
            </script>. All rights reserved.
        </p>
    </div>
</aside>