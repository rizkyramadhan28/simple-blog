<div class="sidebar-box ftco-animate">
    <h3 class="sidebar-heading">Tag Cloud</h3>
    <ul class="tagcloud">
        @foreach ($tags as $tag)
        <a href="{{ route('blog.post.post_tag_list', $tag->slug) }}" class="tag-cloud-link">{{ $tag->name }}</a>
        @endforeach
    </ul>
</div>