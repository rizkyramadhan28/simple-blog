<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('admin.dashboard') }}">Admin Dashboard</a>
        </div>

        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('admin.dashboard') }}">AD</a>
        </div>

        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>

            <li class="dropdown">
                <a href="{{ route('admin.dashboard') }}" class="nav-link"><i
                        class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>

            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-clipboard"></i><span>Category</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('admin.category.index') }}">List Category</a></li>
                    <li><a class="nav-link" href="{{ route('admin.category.create') }}">Add Category</a></li>
                </ul>
            </li>

            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-bookmark"></i><span>Tag</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('admin.tag.index') }}">List Tag</a></li>
                    <li><a class="nav-link" href="{{ route('admin.tag.create') }}">Add Tag</a></li>
                </ul>
            </li>

            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-book-open"></i><span>Post</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('admin.post.index') }}">List Post</a></li>
                    <li><a class="nav-link" href="{{ route('admin.post.create') }}">Add Post</a></li>
                    <li><a class="nav-link" href="{{ route('admin.post.trashed_posts') }}">Trashed Posts</a></li>
                </ul>
            </li>

            @if (Auth::user()->user_role)
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-user"></i><span>List User</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('admin.user.index') }}">List User</a></li>
                </ul>
            </li>
            @endif
        </ul>
    </aside>
</div>