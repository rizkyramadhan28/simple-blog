@extends('layouts.blog')

@section('blog-title')
<title>{{ $post->title }}</title>
@endsection

@section('content')
<h1>{{ $post->title }}</h1>

<p><b>{{ $post->user->name }}</b> on {{ $post->created_at->format("d M Y") }} - {{ $post->category->name }}</p>

<img src="{{ asset('storage/posts/' . $post->image) }}" alt="{{ $post->title }}" class="img-fluid mb-3">

{!! $post->content !!}

<div class="tag-widget post-tag-container mt-3 mb-3">
    <div class="tagcloud">
        @foreach ($post->tags as $tag)
        <a href="#" class="tag-cloud-link">{{ $tag->name }}</a>
        @endforeach
    </div>
</div>

<div class="about-author d-flex p-4 bg-light">
    <div class="bio mr-5">
        <img src="{{ asset('assets/admin/img/avatar/avatar-1.png') }}" alt="Image placeholder" class="img-fluid mb-4">
    </div>

    <div class="desc">
        <h3>{{ $post->user->name }}</h3>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod
            mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit
            cupiditate numquam!</p>
    </div>
</div>
@endsection

@section('custom-js')
<script>
    const imgTag = document.querySelectorAll("img");

    imgTag.forEach(img => {
        if (!img.classList.contains("img-fluid")) {
            img.classList.add("img-fluid");
        }
    });
</script>
@endsection