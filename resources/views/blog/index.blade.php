@extends('layouts.blog')

@section('blog-title')
<title>Rizky Ramadhan</title>
@endsection

@section('content')
@forelse ($posts as $post)
<div class="col-md-12">
    <div class="blog-entry ftco-animate d-md-flex">
        <a href="{{ route('blog.show', [$post->id, $post->slug]) }}" class="img img-2"
            style="background-image: url({{ asset('storage/posts/' . $post->image) }});"></a>

        <div class="text text-2 pl-md-4">
            <h3 class="mb-2"><a href="{{ route('blog.show', [$post->id, $post->slug]) }}">{{ $post->title }}</a>
            </h3>
            <div class="meta-wrap">
                <p class="meta">
                    <span><i class="icon-calendar mr-2"></i>{{
                        $post->created_at->format("d/m/Y") }}</span>
                    <span><i class="icon-user mr-2"></i>{{ $post->user->name
                        }}</span>
                    <span><a href="single.html"><i class="icon-folder-o mr-2"></i>{{
                            $post->category->name }}</a></span>
                </p>
            </div>
            <p class="mb-4">{{ Str::limit(strip_tags($post->content), 100) }}</p>
            <p>
                <a href="#" class="btn-custom">Read More <span class="ion-ios-arrow-forward"></span></a>
            </p>
        </div>
    </div>
</div>
@empty
<h1>Post is empty.</h1>
@endforelse
@endsection

@section('content-pagination')
<div class="row">
    <div class="col">
        <div class="block-27">
            {{ $posts->links() }}
        </div>
    </div>
</div>
@endsection

@section('custom-js')
<script>
    const ul = document.getElementsByClassName("pagination")[0];
    const li = document.querySelectorAll(".pagination li");

    li.forEach(item => {
        item.classList.remove("page-item");

        const span = document.getElementsByClassName("page-link")[0];

        if (item.contains(span)) {
            span.classList.remove("page-link");
        }
    });

    ul.classList.remove("pagination");

    console.log(ul);
</script>
@endsection