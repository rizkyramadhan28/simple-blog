@extends('layouts.admin')

@section('title')
<title>Admin Dashboard</title>
@endsection

@section('content-title')
<h1>List Trashed Post</h1>

<div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
    <div class="breadcrumb-item">List Trashed Post</div>
</div>
@endsection

@section('content')
<div class="card">
    <div class="card-body">

        @if (session("success"))
        <div class="alert alert-success alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>

                {{ session("success") }}
            </div>
        </div>
        @endif

        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Image</th>
                    <th scope="col">Title</th>
                    <th scope="col">Tags</th>
                    <th scope="col">Category</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>

            <tbody>
                @forelse ($posts as $post)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td><img class="rounded" width="100px" src="{{ asset('storage/posts/' . $post->image) }}"
                            alt="Image">
                    </td>
                    <td>{{ Str::limit($post->title, 15) }}</td>
                    <td>
                        <ul>
                            @foreach ($post->tags as $tag)
                            <li style="list-style: none"><span class="badge badge-primary">{{ $tag->name }}</span></li>
                            @endforeach
                        </ul>
                    </td>
                    <td>{{ Str::limit($post->category->name, 15) }}</td>
                    <td>
                        <form action="{{ route('admin.post.destroy', $post->id) }}" method="POST">
                            @csrf
                            @method("DELETE")

                            <a href="{{ route('admin.post.restore', $post->id) }}" class="btn btn-primary btn-sm"><i
                                    class="far fa-edit"></i> Restore</a>

                            <button type="submit" id="swal-6" class="btn btn-danger btn-sm"><i class="fas fa-times"></i>
                                Delete</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="6" class="text-center">Post is empty.</td>
                </tr>
                @endforelse
            </tbody>
        </table>

        <div class="row float-right">
            <div class="col">
                {{ $posts->links() }}
            </div>
        </div>
    </div>
</div>
@endsection