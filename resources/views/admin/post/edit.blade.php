@extends('layouts.admin')

@section('title')
<title>Admin Dashboard</title>
@endsection

@section('css-libraries')
<link rel="stylesheet" href="{{ asset('assets/admin/modules/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/modules/jquery-selectric/selectric.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/modules/summernote/summernote-bs4.css') }}">
@endsection

@section('content-title')
<h1>Edit Post</h1>

<div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
    <div class="breadcrumb-item">Edit Post</div>
</div>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">
            <h4>Edit Post</h4>
        </div>
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col">
                <form action="{{ route('admin.post.update', $post->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method("PUT")

                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" placeholder="Enter post title"
                            value="{{ $post->title }}" required>

                        <p class="text-danger">{{ $errors->first("title") }}</p>
                    </div>

                    <div class="form-group">
                        <label>Category</label>

                        <select name="category_id" class="form-control" required>
                            @foreach ($categories as $category)
                            <option value="{{ $category->id }}"
                                {{ $post->category_id == $category->id ? 'selected' : '' }}>
                                {{ $category->name }}</option>
                            @endforeach
                        </select>

                        <p class="text-danger">{{ $errors->first("category_id") }}</p>
                    </div>

                    <div class="form-group">
                        <label>Tags</label>
                        <select class="form-control select2" multiple="" name="tags[]">
                            @foreach ($tags as $tag)
                            <option value="{{ $tag->id }}" @foreach ($post->tags as $postTag)
                                {{ $postTag->id == $tag->id ? 'selected' : '' }}
                                @endforeach>{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Content</label>
                        <div>
                            <textarea name="content" class="summernote" placeholder="Enter post content"
                                value="{{ $post->content }}" required>{{ $post->content }}</textarea>

                            <p class="text-danger">{{ $errors->first("content") }}</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image">Image</label>

                        <div class="mb-2">
                            <img src="{{ asset('storage/posts/' . $post->image) }}" width="500px" class="img-fluid">
                        </div>

                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="image" />

                                <label class="custom-file-label" for="image">Choose image</label>
                            </div>
                        </div>

                        <p><b>Leave it empty if you doesn't want to update the image.</b></p>

                        <p class="text-danger">{{ $errors->first("image") }}</p>
                    </div>

                    <div class="card-footer text-right">
                        <button class="btn btn-primary mr-1" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js-libraries')
<script src="{{ asset('assets/admin/modules/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/admin/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
<script src="{{ asset('assets/admin/modules/summernote/summernote-bs4.js') }}"></script>
@endsection

@section('custom-js')
<script>
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
@endsection