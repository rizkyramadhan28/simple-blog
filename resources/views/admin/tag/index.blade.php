@extends('layouts.admin')

@section('title')
<title>Admin Dashboard</title>
@endsection

@section('content-title')
<h1>List Tag</h1>

<div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
    <div class="breadcrumb-item">List Tag</div>
</div>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">
            <a href="{{ route('admin.tag.create') }}" class="btn btn-icon icon-left btn-primary"><i
                    class="far fa-edit"></i> Add Tag</a>
        </div>
    </div>

    <div class="card-body">

        @if (session("success"))
        <div class="alert alert-success alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>

                {{ session("success") }}
            </div>
        </div>
        @endif

        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Updated At</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>

            <tbody>
                @forelse ($tags as $tag)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $tag->name }}</td>
                    <td>{{ $tag->updated_at->format("d-m-Y") }}</td>
                    <td>
                        <form action="{{ route('admin.tag.destroy', $tag->id) }}" method="POST">
                            @csrf
                            @method("DELETE")

                            <a href="{{ route('admin.tag.edit', $tag->id) }}" class="btn btn-warning btn-sm"><i
                                    class="far fa-edit"></i> Edit</a>

                            <button type="submit" id="swal-6" class="btn btn-danger btn-sm"><i class="fas fa-times"></i>
                                Delete</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4" class="text-center">Tag is empty.</td>
                </tr>
                @endforelse
            </tbody>
        </table>

        <div class="row float-right">
            <div class="col">
                {{ $tags->links() }}
            </div>
        </div>
    </div>
</div>
@endsection