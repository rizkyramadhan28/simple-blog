@extends('layouts.admin')

@section('title')
<title>Admin Dashboard</title>
@endsection

@section('content-title')
<h1>Edit Tag</h1>

<div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
    <div class="breadcrumb-item">Edit Tag</div>
</div>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">
            <h4>Edit Tag</h4>
        </div>
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col">
                <form action="{{ route('admin.tag.update', $tag->id) }}" method="POST">
                    @csrf
                    @method("PUT")

                    <div class="form-group">
                        <label>Tag Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter tag name"
                            value="{{ $tag->name }}" required>

                        <p class="text-danger">{{ $errors->first("name") }}</p>
                    </div>

                    <div class="card-footer text-right">
                        <button class="btn btn-primary mr-1" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection