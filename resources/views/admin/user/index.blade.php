@extends('layouts.admin')

@section('title')
<title>Admin Dashboard</title>
@endsection

@section('content-title')
<h1>List User</h1>

<div class="section-header-breadcrumb">
    <div class="breadcrumb-item active"><a href="{{ route('admin.dashboard') }}">Dashboard</a></div>
    <div class="breadcrumb-item">List User</div>
</div>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">
            <a href="{{ route('admin.user.create') }}" class="btn btn-icon icon-left btn-primary"><i
                    class="far fa-edit"></i> Add User</a>
        </div>
    </div>

    <div class="card-body">

        @if (session("success"))
        <div class="alert alert-success alert-dismissible show fade">
            <div class="alert-body">
                <button class="close" data-dismiss="alert">
                    <span>×</span>
                </button>

                {{ session("success") }}
            </div>
        </div>
        @endif

        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Role</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>

            <tbody>
                @forelse($users as $user)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $user->name }}</td>
                    <td>@if ($user->user_role)
                        <span class="badge badge-success">Admin</span>
                        @else
                        <span class="badge badge-primary">Author</span>
                        @endif</td>
                    <td>
                        <form action="{{ route('admin.user.destroy', $user->id) }}" method="POST">
                            @csrf
                            @method("DELETE")

                            <a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-warning btn-sm"><i
                                    class="far fa-edit"></i> Edit</a>

                            <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-times"></i>
                                Delete</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="3" class="text-center">User is empty.</td>
                </tr>
                @endforelse
            </tbody>
        </table>

        <div class="row float-right">
            <div class="col">
                {{ $users->links() }}
            </div>
        </div>
    </div>
</div>
@endsection