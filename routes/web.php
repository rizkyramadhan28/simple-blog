<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "BlogController@index")->name("blog.index");

Route::group(["prefix" => "articles"], function() {
    Route::get("/categories/{category}", "BlogController@post_category_list")->name("blog.post.category_list");
    Route::get("/tags/{tag}", "BlogController@post_tag_list")->name("blog.post.post_tag_list");
    Route::get("/search", "BlogController@search")->name("blog.post.search");
    Route::get("/{post}/{slug}", "BlogController@show")->name("blog.show");
});

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);

Route::group(["prefix" => "admin", "middleware" => "auth"], function() {
    Route::get('/home', 'HomeController@index')->name("admin.dashboard");
    
    Route::resource("category", "CategoryController", ["as" => "admin"])->except(["show"]);
    Route::resource("tag", "TagController", ["as" => "admin"])->except(["show"]);
    Route::resource("user", "UserController", ["as" => "admin"])->except(["show"]);
    Route::get("/post/trashed", "PostController@trashed_posts")->name("admin.post.trashed_posts");
    Route::get("/post/trashed/restore/{post}", "PostController@restore")->name("admin.post.restore");
    Route::delete("/post/trashed/{post}", "PostController@soft_delete")->name("admin.post.soft_delete");
    Route::resource("post", "PostController", ["as" => "admin"]);
});